import os
import requests
import bs4
from bs4 import BeautifulSoup
from tm1637 import TM1637
from time import sleep

requete = requests.get('http://www.meteocity.com/france/arles_v13004/')
page = requete.content
soup = BeautifulSoup(page, 'html.parser')

counter_box = soup.find('span', attrs={'class': 'tempMax'})
counter = counter_box.text

print ("temperature max aujourd'hui: ", counter, "°C")

def afficher_text(aff):
    # Affichage sur le module
    aff.temperature(int(counter))

afficheur = TM1637(clk=23, dio=24)
afficheur.brightness(7)

for i in range(0, 1):
    afficher_text(afficheur)
    sleep(1)
